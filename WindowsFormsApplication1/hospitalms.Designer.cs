﻿namespace WindowsFormsApplication1
{
    partial class hospitalms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(hospitalms));
            this.btnexit = new System.Windows.Forms.Button();
            this.btnpatientlist = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnappointment = new System.Windows.Forms.Button();
            this.btndoctorlist = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnexit
            // 
            this.btnexit.BackColor = System.Drawing.Color.Transparent;
            this.btnexit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnexit.FlatAppearance.BorderSize = 0;
            this.btnexit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnexit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnexit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnexit.ForeColor = System.Drawing.Color.Transparent;
            this.btnexit.Image = ((System.Drawing.Image)(resources.GetObject("btnexit.Image")));
            this.btnexit.Location = new System.Drawing.Point(555, 308);
            this.btnexit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(113, 87);
            this.btnexit.TabIndex = 0;
            this.btnexit.UseVisualStyleBackColor = false;
            this.btnexit.Click += new System.EventHandler(this.exitbtn_Click);
            // 
            // btnpatientlist
            // 
            this.btnpatientlist.BackColor = System.Drawing.Color.Transparent;
            this.btnpatientlist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnpatientlist.FlatAppearance.BorderSize = 0;
            this.btnpatientlist.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnpatientlist.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnpatientlist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpatientlist.ForeColor = System.Drawing.Color.Transparent;
            this.btnpatientlist.Image = ((System.Drawing.Image)(resources.GetObject("btnpatientlist.Image")));
            this.btnpatientlist.Location = new System.Drawing.Point(349, 46);
            this.btnpatientlist.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnpatientlist.Name = "btnpatientlist";
            this.btnpatientlist.Size = new System.Drawing.Size(160, 144);
            this.btnpatientlist.TabIndex = 1;
            this.btnpatientlist.UseVisualStyleBackColor = false;
            this.btnpatientlist.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnadd
            // 
            this.btnadd.BackColor = System.Drawing.Color.Transparent;
            this.btnadd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnadd.FlatAppearance.BorderSize = 0;
            this.btnadd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnadd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnadd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnadd.ForeColor = System.Drawing.Color.Transparent;
            this.btnadd.Image = ((System.Drawing.Image)(resources.GetObject("btnadd.Image")));
            this.btnadd.Location = new System.Drawing.Point(107, 47);
            this.btnadd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(141, 126);
            this.btnadd.TabIndex = 2;
            this.btnadd.UseVisualStyleBackColor = false;
            this.btnadd.Click += new System.EventHandler(this.addbtn_Click);
            // 
            // btnappointment
            // 
            this.btnappointment.BackColor = System.Drawing.Color.Transparent;
            this.btnappointment.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnappointment.FlatAppearance.BorderSize = 0;
            this.btnappointment.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnappointment.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnappointment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnappointment.ForeColor = System.Drawing.Color.Transparent;
            this.btnappointment.Image = ((System.Drawing.Image)(resources.GetObject("btnappointment.Image")));
            this.btnappointment.Location = new System.Drawing.Point(99, 223);
            this.btnappointment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnappointment.Name = "btnappointment";
            this.btnappointment.Size = new System.Drawing.Size(168, 146);
            this.btnappointment.TabIndex = 3;
            this.btnappointment.UseVisualStyleBackColor = false;
            this.btnappointment.Click += new System.EventHandler(this.btnappointment_Click);
            // 
            // btndoctorlist
            // 
            this.btndoctorlist.BackColor = System.Drawing.Color.Transparent;
            this.btndoctorlist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btndoctorlist.FlatAppearance.BorderSize = 0;
            this.btndoctorlist.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btndoctorlist.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btndoctorlist.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndoctorlist.ForeColor = System.Drawing.Color.Transparent;
            this.btndoctorlist.Image = ((System.Drawing.Image)(resources.GetObject("btndoctorlist.Image")));
            this.btndoctorlist.Location = new System.Drawing.Point(332, 222);
            this.btndoctorlist.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btndoctorlist.Name = "btndoctorlist";
            this.btndoctorlist.Size = new System.Drawing.Size(177, 148);
            this.btndoctorlist.TabIndex = 4;
            this.btndoctorlist.UseVisualStyleBackColor = false;
            this.btndoctorlist.Click += new System.EventHandler(this.btndoctorlist_Click);
            // 
            // hospitalms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 410);
            this.ControlBox = false;
            this.Controls.Add(this.btndoctorlist);
            this.Controls.Add(this.btnappointment);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.btnpatientlist);
            this.Controls.Add(this.btnexit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "hospitalms";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hospital Management System";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.hospitalms_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnpatientlist;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnappointment;
        private System.Windows.Forms.Button btndoctorlist;
    }
}