﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace WindowsFormsApplication1
{
    public partial class patientlist : Form
    {
        public patientlist()
        {
            InitializeComponent();
        }

        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        //DataSet ds = new System.Data.DataSet();
        DataSet ds = new DataSet();
        void list()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("SElect *from patientlist", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "patientlist");
            dataGridView1.DataSource = ds.Tables["patientlist"];
            Connection.Close();
        }

        private void patientlist_Load(object sender, EventArgs e)
        {
            list();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("Select *from patientlist where patientname like '" + txtsearch.Text + "%'", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "patientlist");
            dataGridView1.DataSource = ds.Tables["patientlist"];
            Connection.Close();
        }

        private void patientlist_FormClosed(object sender, FormClosedEventArgs e)
        {
            hospitalms f1 = new hospitalms();
            f1.Show();
        }

        private void btndelete_Click(object sender, EventArgs e)
        {
          DialogResult result = MessageBox.Show("Are you sure that you want To delete ?", "Warning", MessageBoxButtons.YesNo,
              MessageBoxIcon.Question);

          if (result == DialogResult.Yes) { 
            Connection.Open();
            Command.Connection = Connection;
            Command.CommandText = "DELETE FROM patientlist WHERE idnumber='" + dataGridView1.CurrentRow.Cells[1].Value.ToString() + "'";
            Command.ExecuteNonQuery();
            Connection.Close();
            list();
            
          }   
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {    
            Connection.Open();
            Command.Connection = Connection;
            Command.CommandText = "UPDATE patientlist set  idnumber='" + txtid.Text + "',patientname='" + txtname.Text +
                "',patientsurname='" + txtsurname.Text + "',gender='" + comboGender.SelectedItem.ToString() +
                "',mothername='" + txtmother.Text + "',fathername='" + txtfather.Text + "',placeofbirth='" + txtplace.Text +
                "',dateofbirth='" + txtdate.Text + "',cellphone='" + txtcell.Text + "',homephone='" + txthome.Text + "',email='" + txtemail.Text +
                "',address='" + txtaddress.Text + "',department='" + comboDepartment.SelectedItem.ToString() +
                "',doctor='" + comboDoctor.SelectedItem.ToString() + "',appointmentdate='" + dateTimeAppointment.Text +
                "',appointmenttime='" + listBoxTime.SelectedItem.ToString() + "' WHERE idnumber='" + txtid.Text + "'";
            Command.ExecuteNonQuery();
            Connection.Close();
            MessageBox.Show("Registration Successful.","Registration Completed",MessageBoxButtons.OK,MessageBoxIcon.Information);
            list();
            
        }
        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtid.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            txtname.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            txtsurname.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            comboGender.SelectedItem = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            txtmother.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            txtfather.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            txtplace.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            txtdate.Text = dataGridView1.CurrentRow.Cells[8].Value.ToString();
            txtcell.Text = dataGridView1.CurrentRow.Cells[9].Value.ToString();
            txthome.Text = dataGridView1.CurrentRow.Cells[10].Value.ToString();
            txtemail.Text = dataGridView1.CurrentRow.Cells[11].Value.ToString();
            txtaddress.Text = dataGridView1.CurrentRow.Cells[12].Value.ToString();
            comboDepartment.SelectedItem = dataGridView1.CurrentRow.Cells[13].Value.ToString();
            comboDoctor.SelectedItem = dataGridView1.CurrentRow.Cells[14].Value.ToString();
            dateTimeAppointment.Text = dataGridView1.CurrentRow.Cells[15].Value.ToString();
            listBoxTime.SelectedItem = dataGridView1.CurrentRow.Cells[16].Value.ToString();
        }

       
        private void txtsearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

     
   }
}
