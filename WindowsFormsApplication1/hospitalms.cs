﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class hospitalms : Form
    {
        public hospitalms()
        {
            InitializeComponent();
        }

        private void exitbtn_Click(object sender, EventArgs e)
        {
            this.Close();
            Login f4 = new Login();
            f4.Show();
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            this.Close();
            addingpatient f5 = new addingpatient();
            f5.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            patientlist f6 = new patientlist();
            f6.Show();
        }

        private void btnappointment_Click(object sender, EventArgs e)
        {
            this.Close();
            appointment f7 = new appointment();
            f7.Show();
        }

        private void btndoctorlist_Click(object sender, EventArgs e)
        {
            this.Close();
            informationsofdoctors f8 = new informationsofdoctors();
            f8.Show();
        }
        private void hospitalms_FormClosed(object sender, FormClosedEventArgs e)
        {
         
        }

    }
}
