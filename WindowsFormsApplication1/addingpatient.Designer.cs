﻿namespace WindowsFormsApplication1
{
    partial class addingpatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addingpatient));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboGender = new System.Windows.Forms.ComboBox();
            this.txtid = new System.Windows.Forms.TextBox();
            this.txtdate = new System.Windows.Forms.TextBox();
            this.txtplace = new System.Windows.Forms.TextBox();
            this.txtfather = new System.Windows.Forms.TextBox();
            this.txtmother = new System.Windows.Forms.TextBox();
            this.txtsurname = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dateofbirth = new System.Windows.Forms.Label();
            this.placeofbirth = new System.Windows.Forms.Label();
            this.fathername = new System.Windows.Forms.Label();
            this.mothername = new System.Windows.Forms.Label();
            this.gender = new System.Windows.Forms.Label();
            this.surname = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtaddress = new System.Windows.Forms.TextBox();
            this.Address = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txthome = new System.Windows.Forms.TextBox();
            this.txtcell = new System.Windows.Forms.TextBox();
            this.cellphone = new System.Windows.Forms.Label();
            this.homephone = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBoxTime = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboDoctor = new System.Windows.Forms.ComboBox();
            this.comboDepartment = new System.Windows.Forms.ComboBox();
            this.Date = new System.Windows.Forms.Label();
            this.dateTimeAppointment = new System.Windows.Forms.DateTimePicker();
            this.Doctor = new System.Windows.Forms.Label();
            this.Department = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboGender);
            this.groupBox1.Controls.Add(this.txtid);
            this.groupBox1.Controls.Add(this.txtdate);
            this.groupBox1.Controls.Add(this.txtplace);
            this.groupBox1.Controls.Add(this.txtfather);
            this.groupBox1.Controls.Add(this.txtmother);
            this.groupBox1.Controls.Add(this.txtsurname);
            this.groupBox1.Controls.Add(this.txtname);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dateofbirth);
            this.groupBox1.Controls.Add(this.placeofbirth);
            this.groupBox1.Controls.Add(this.fathername);
            this.groupBox1.Controls.Add(this.mothername);
            this.groupBox1.Controls.Add(this.gender);
            this.groupBox1.Controls.Add(this.surname);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(399, 384);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ID Informations";
            // 
            // comboGender
            // 
            this.comboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboGender.FormattingEnabled = true;
            this.comboGender.Items.AddRange(new object[] {
            "Female",
            "Male"});
            this.comboGender.Location = new System.Drawing.Point(192, 167);
            this.comboGender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboGender.Name = "comboGender";
            this.comboGender.Size = new System.Drawing.Size(197, 30);
            this.comboGender.TabIndex = 4;
            // 
            // txtid
            // 
            this.txtid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtid.Location = new System.Drawing.Point(192, 44);
            this.txtid.Margin = new System.Windows.Forms.Padding(4);
            this.txtid.MaxLength = 11;
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(197, 30);
            this.txtid.TabIndex = 1;
            this.txtid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtid_KeyPress);
            // 
            // txtdate
            // 
            this.txtdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtdate.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtdate.Location = new System.Drawing.Point(192, 343);
            this.txtdate.Margin = new System.Windows.Forms.Padding(4);
            this.txtdate.MaxLength = 10;
            this.txtdate.Name = "txtdate";
            this.txtdate.Size = new System.Drawing.Size(197, 30);
            this.txtdate.TabIndex = 8;
            this.txtdate.Text = "DD.MM.YYYY";
            this.txtdate.Click += new System.EventHandler(this.txtdate_Click);
            // 
            // txtplace
            // 
            this.txtplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtplace.Location = new System.Drawing.Point(192, 300);
            this.txtplace.Margin = new System.Windows.Forms.Padding(4);
            this.txtplace.Name = "txtplace";
            this.txtplace.Size = new System.Drawing.Size(197, 30);
            this.txtplace.TabIndex = 7;
            this.txtplace.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtplace_KeyPress);
            // 
            // txtfather
            // 
            this.txtfather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtfather.Location = new System.Drawing.Point(192, 255);
            this.txtfather.Margin = new System.Windows.Forms.Padding(4);
            this.txtfather.Name = "txtfather";
            this.txtfather.Size = new System.Drawing.Size(197, 30);
            this.txtfather.TabIndex = 6;
            this.txtfather.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtfather_KeyPress);
            // 
            // txtmother
            // 
            this.txtmother.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtmother.Location = new System.Drawing.Point(192, 208);
            this.txtmother.Margin = new System.Windows.Forms.Padding(4);
            this.txtmother.Name = "txtmother";
            this.txtmother.Size = new System.Drawing.Size(197, 30);
            this.txtmother.TabIndex = 5;
            this.txtmother.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmother_KeyPress);
            // 
            // txtsurname
            // 
            this.txtsurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtsurname.Location = new System.Drawing.Point(192, 130);
            this.txtsurname.Margin = new System.Windows.Forms.Padding(4);
            this.txtsurname.Name = "txtsurname";
            this.txtsurname.Size = new System.Drawing.Size(197, 30);
            this.txtsurname.TabIndex = 3;
            this.txtsurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsurname_KeyPress);
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtname.Location = new System.Drawing.Point(192, 87);
            this.txtname.Margin = new System.Windows.Forms.Padding(4);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(197, 30);
            this.txtname.TabIndex = 2;
            this.txtname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtname_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(8, 48);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 25);
            this.label8.TabIndex = 7;
            this.label8.Text = "Id Number:";
            // 
            // dateofbirth
            // 
            this.dateofbirth.AutoSize = true;
            this.dateofbirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateofbirth.Location = new System.Drawing.Point(8, 347);
            this.dateofbirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dateofbirth.Name = "dateofbirth";
            this.dateofbirth.Size = new System.Drawing.Size(124, 25);
            this.dateofbirth.TabIndex = 6;
            this.dateofbirth.Text = "Date of Birth:";
            // 
            // placeofbirth
            // 
            this.placeofbirth.AutoSize = true;
            this.placeofbirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.placeofbirth.Location = new System.Drawing.Point(8, 304);
            this.placeofbirth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.placeofbirth.Name = "placeofbirth";
            this.placeofbirth.Size = new System.Drawing.Size(132, 25);
            this.placeofbirth.TabIndex = 5;
            this.placeofbirth.Text = "Place of Birth:";
            // 
            // fathername
            // 
            this.fathername.AutoSize = true;
            this.fathername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.fathername.Location = new System.Drawing.Point(8, 258);
            this.fathername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.fathername.Name = "fathername";
            this.fathername.Size = new System.Drawing.Size(131, 25);
            this.fathername.TabIndex = 4;
            this.fathername.Text = "Father Name:";
            // 
            // mothername
            // 
            this.mothername.AutoSize = true;
            this.mothername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.mothername.Location = new System.Drawing.Point(8, 212);
            this.mothername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mothername.Name = "mothername";
            this.mothername.Size = new System.Drawing.Size(136, 25);
            this.mothername.TabIndex = 3;
            this.mothername.Text = "Mother Name:";
            // 
            // gender
            // 
            this.gender.AutoSize = true;
            this.gender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gender.Location = new System.Drawing.Point(8, 171);
            this.gender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(83, 25);
            this.gender.TabIndex = 2;
            this.gender.Text = "Gender:";
            // 
            // surname
            // 
            this.surname.AutoSize = true;
            this.surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.surname.Location = new System.Drawing.Point(8, 134);
            this.surname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.surname.Name = "surname";
            this.surname.Size = new System.Drawing.Size(98, 25);
            this.surname.TabIndex = 1;
            this.surname.Text = "Surname:";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.name.Location = new System.Drawing.Point(8, 91);
            this.name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(70, 25);
            this.name.TabIndex = 0;
            this.name.Text = "Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtaddress);
            this.groupBox2.Controls.Add(this.Address);
            this.groupBox2.Controls.Add(this.txtemail);
            this.groupBox2.Controls.Add(this.txthome);
            this.groupBox2.Controls.Add(this.txtcell);
            this.groupBox2.Controls.Add(this.cellphone);
            this.groupBox2.Controls.Add(this.homephone);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox2.Location = new System.Drawing.Point(423, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(416, 384);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Personal Informations";
            // 
            // txtaddress
            // 
            this.txtaddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtaddress.Location = new System.Drawing.Point(173, 186);
            this.txtaddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtaddress.Multiline = true;
            this.txtaddress.Name = "txtaddress";
            this.txtaddress.Size = new System.Drawing.Size(233, 141);
            this.txtaddress.TabIndex = 12;
            // 
            // Address
            // 
            this.Address.AutoSize = true;
            this.Address.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Address.Location = new System.Drawing.Point(8, 186);
            this.Address.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(91, 25);
            this.Address.TabIndex = 7;
            this.Address.Text = "Address:";
            // 
            // txtemail
            // 
            this.txtemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtemail.Location = new System.Drawing.Point(173, 130);
            this.txtemail.Margin = new System.Windows.Forms.Padding(4);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(233, 30);
            this.txtemail.TabIndex = 11;
            // 
            // txthome
            // 
            this.txthome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txthome.Location = new System.Drawing.Point(173, 87);
            this.txthome.Margin = new System.Windows.Forms.Padding(4);
            this.txthome.MaxLength = 10;
            this.txthome.Name = "txthome";
            this.txthome.Size = new System.Drawing.Size(233, 30);
            this.txthome.TabIndex = 13;
            this.txthome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txthome_KeyPress);
            // 
            // txtcell
            // 
            this.txtcell.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtcell.Location = new System.Drawing.Point(173, 48);
            this.txtcell.Margin = new System.Windows.Forms.Padding(4);
            this.txtcell.MaxLength = 10;
            this.txtcell.Name = "txtcell";
            this.txtcell.Size = new System.Drawing.Size(233, 30);
            this.txtcell.TabIndex = 9;
            this.txtcell.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcell_KeyPress);
            // 
            // cellphone
            // 
            this.cellphone.AutoSize = true;
            this.cellphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cellphone.Location = new System.Drawing.Point(8, 52);
            this.cellphone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cellphone.Name = "cellphone";
            this.cellphone.Size = new System.Drawing.Size(114, 25);
            this.cellphone.TabIndex = 1;
            this.cellphone.Text = "Cell Phone:";
            // 
            // homephone
            // 
            this.homephone.AutoSize = true;
            this.homephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.homephone.Location = new System.Drawing.Point(7, 91);
            this.homephone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.homephone.Name = "homephone";
            this.homephone.Size = new System.Drawing.Size(132, 25);
            this.homephone.TabIndex = 2;
            this.homephone.Text = "Home Phone:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(8, 134);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "E-mail:";
            // 
            // btnsave
            // 
            this.btnsave.BackColor = System.Drawing.Color.Transparent;
            this.btnsave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsave.FlatAppearance.BorderSize = 0;
            this.btnsave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnsave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnsave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnsave.Image = ((System.Drawing.Image)(resources.GetObject("btnsave.Image")));
            this.btnsave.Location = new System.Drawing.Point(1212, 402);
            this.btnsave.Margin = new System.Windows.Forms.Padding(4);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(47, 43);
            this.btnsave.TabIndex = 2;
            this.btnsave.UseVisualStyleBackColor = false;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 454);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1315, 302);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBoxTime);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboDoctor);
            this.groupBox3.Controls.Add(this.comboDepartment);
            this.groupBox3.Controls.Add(this.Date);
            this.groupBox3.Controls.Add(this.dateTimeAppointment);
            this.groupBox3.Controls.Add(this.Doctor);
            this.groupBox3.Controls.Add(this.Department);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(860, 15);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(471, 383);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Appointment Informations";
            // 
            // listBoxTime
            // 
            this.listBoxTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.listBoxTime.FormattingEnabled = true;
            this.listBoxTime.ItemHeight = 25;
            this.listBoxTime.Items.AddRange(new object[] {
            "8:30",
            "9:30",
            "10:30",
            "11:30",
            "12:30",
            "13:30",
            "14:30",
            "15:30"});
            this.listBoxTime.Location = new System.Drawing.Point(175, 217);
            this.listBoxTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBoxTime.Name = "listBoxTime";
            this.listBoxTime.Size = new System.Drawing.Size(140, 129);
            this.listBoxTime.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(27, 212);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 25);
            this.label1.TabIndex = 10;
            this.label1.Text = "Time:";
            // 
            // comboDoctor
            // 
            this.comboDoctor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDoctor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboDoctor.FormattingEnabled = true;
            this.comboDoctor.Items.AddRange(new object[] {
            "Prof. Dr. Gazi Yaşargil",
            "Prof. Dr. Hande Özdinler",
            "Prof.Dr. Ömer Özkan",
            "Dr. Murat Digiçaylıoğlu",
            "Prof. Dr. Tayfun Aybek",
            "Prof. Dr. Murat Günel",
            "Prof. Dr. Tanju Atamer",
            "Prof. Dr. Ali Gürçay",
            "Prof. Dr. Faruk Aykan",
            "Dr. Aydoğan Öbek",
            "Dr. Ayda Uluhan",
            "Prof. Dr. Ali Oto",
            "Prof. Dr. Çetin Erol ",
            "Prof. Dr. Derviş Oral ",
            "Prof. Dr. Ferhan Özmen ",
            "Prof. Dr. Kamil Adalet"});
            this.comboDoctor.Location = new System.Drawing.Point(175, 94);
            this.comboDoctor.Margin = new System.Windows.Forms.Padding(4);
            this.comboDoctor.Name = "comboDoctor";
            this.comboDoctor.Size = new System.Drawing.Size(272, 33);
            this.comboDoctor.TabIndex = 14;
            // 
            // comboDepartment
            // 
            this.comboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDepartment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.comboDepartment.FormattingEnabled = true;
            this.comboDepartment.Items.AddRange(new object[] {
            "Anesthetics",
            "Cardiology",
            "Gastroenterology",
            "Gynecology",
            "Microbiology",
            "Neurology",
            "Nutrition and Dietetics",
            "Oncology",
            "Orthopaedics",
            "Otolaryngology (Ear, Nose, and Throat)",
            "Physiotherapy",
            "Radiology",
            "Radiotherapy",
            "Urology"});
            this.comboDepartment.Location = new System.Drawing.Point(175, 46);
            this.comboDepartment.Margin = new System.Windows.Forms.Padding(4);
            this.comboDepartment.Name = "comboDepartment";
            this.comboDepartment.Size = new System.Drawing.Size(272, 33);
            this.comboDepartment.TabIndex = 13;
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Date.Location = new System.Drawing.Point(27, 156);
            this.Date.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(59, 25);
            this.Date.TabIndex = 3;
            this.Date.Text = "Date:";
            // 
            // dateTimeAppointment
            // 
            this.dateTimeAppointment.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeAppointment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dateTimeAppointment.Location = new System.Drawing.Point(175, 150);
            this.dateTimeAppointment.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimeAppointment.Name = "dateTimeAppointment";
            this.dateTimeAppointment.Size = new System.Drawing.Size(272, 30);
            this.dateTimeAppointment.TabIndex = 15;
            // 
            // Doctor
            // 
            this.Doctor.AutoSize = true;
            this.Doctor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Doctor.Location = new System.Drawing.Point(27, 97);
            this.Doctor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Doctor.Name = "Doctor";
            this.Doctor.Size = new System.Drawing.Size(75, 25);
            this.Doctor.TabIndex = 1;
            this.Doctor.Text = "Doctor:";
            // 
            // Department
            // 
            this.Department.AutoSize = true;
            this.Department.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Department.Location = new System.Drawing.Point(27, 48);
            this.Department.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Department.Name = "Department";
            this.Department.Size = new System.Drawing.Size(119, 25);
            this.Department.TabIndex = 0;
            this.Department.Text = "Department:";
            // 
            // addingpatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1371, 750);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "addingpatient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adding Patient";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.addingpatient_FormClosed);
            this.Load += new System.EventHandler(this.addingpatient_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label dateofbirth;
        private System.Windows.Forms.Label placeofbirth;
        private System.Windows.Forms.Label fathername;
        private System.Windows.Forms.Label mothername;
        private System.Windows.Forms.Label gender;
        private System.Windows.Forms.Label surname;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.TextBox txtdate;
        private System.Windows.Forms.TextBox txtplace;
        private System.Windows.Forms.TextBox txtfather;
        private System.Windows.Forms.TextBox txtmother;
        private System.Windows.Forms.TextBox txtsurname;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txthome;
        private System.Windows.Forms.TextBox txtcell;
        private System.Windows.Forms.Label cellphone;
        private System.Windows.Forms.Label homephone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtaddress;
        private System.Windows.Forms.Label Address;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.DateTimePicker dateTimeAppointment;
        private System.Windows.Forms.Label Doctor;
        private System.Windows.Forms.Label Department;
        private System.Windows.Forms.ComboBox comboDoctor;
        private System.Windows.Forms.ComboBox comboDepartment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboGender;
        private System.Windows.Forms.ListBox listBoxTime;
    }
}