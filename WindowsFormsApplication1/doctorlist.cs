﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;


namespace WindowsFormsApplication1
{
    public partial class informationsofdoctors : Form
    {
        public informationsofdoctors()
        {
            InitializeComponent();
        }
        private void informationsofdoctors_Load(object sender, EventArgs e)
        {

        }

        private void doctorlist_FormClosed(object sender, FormClosedEventArgs e)
        {
            hospitalms f10 = new hospitalms();
            f10.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
             //anesthetics
            if ("Anesthetics".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Prof. Dr. Ali Yıldırım".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + " From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-14.30 \nRoom :314 \nFloor: 3  \nPhone:212 aaa aa aa ");
                }
                else if ("Prof. Dr. Kemal Söyler".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :14.30-18.30 \nRoom :315 \nFloor: 3  \nPhone: 212 bbb bb bb");
                }
                else if ("Prof. Dr. Kadir Yılmaz".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :18.30-22.30 \nRoom :317 \nFloor: 3  \nPhone: 212 ccc cc cc");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }

                //Neurology
            else if ("Neurology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Mert Kılıç".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :214 \nFloor: 2  \nPhone: 212 ddd dd dd");
                }
                else if ("Ömer Arslan".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :16.30-00.30 \nRoom :215 \nFloor: 2  \nPhone: 212 eee ee ee");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //cardiology
            else if ("Cardiology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Prof. Dr. Gazi Yaşargil".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-12.30 \nRoom :412 \nFloor: 4  \nPhone: 212 fff ff ff");
                }
                else if ("Prof. Dr. Hande Özdinler".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :12.30-16.30 \nRoom :413 \nFloor: 4  \nPhone: 212 ggg gg gg");
                }
                else if ("Prof.Dr. Ömer Özkan".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :16.30-20.30 \nRoom :414 \nFloor: 4  \nPhone: 212 hhh hh hh");
                }
                else if ("Dr. Murat Digiçaylıoğlu".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :20.30-00.30 \nRoom :415 \nFloor: 4  \nPhone: 212 jjj jj jj");
                }
                else if ("Dr. Murat Digiçaylıoğlu".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :00.30-07.30 \nRoom :416 \nFloor: 4  \nPhone: 212 kkk kk kk");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Orthopaedics
            else if ("Orthopaedics".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Prof. Dr. Tayfun Aybek".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :8.30-12.30 \nRoom :512 \nFloor: 5  \nPhone: 212 lll ll ll");
                }
                else if ("Prof. Dr. Murat Günel".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :12.30-16.30 \nRoom :513 \nFloor: 5  \nPhone: 212 mmm mm mm");
                }
                else if ("Prof. Dr. Tanju Atamer".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :16.30-20.30 \nRoom :514 \nFloor: 5  \nPhone: 212 nnn nn nn");
                }
                else if ("Prof. Dr. Ali Gürçay".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :20.30-00.30 \nRoom :515 \nFloor: 5  \nPhone: 212 ooo oo oo");
                }
                else if ("Prof. Dr. Faruk Aykan".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :00.30-07.30 \nRoom :516 \nFloor: 5  \nPhone: 212 ppp pp pp");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Otolaryngology
            else if ("Otolaryngology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Dr. Aydoğan Öbek".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :8.30-12.30 \nRoom :612 \nFloor: 6  \nPhone: 212 rrr rr rr");
                }
                else if ("Dr. Ayda Uluhan".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :12.30-16.30 \nRoom :613 \nFloor: 6  \nPhone: 212 sss ss ss");
                }
                else if ("Prof. Dr. Ali Oto".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :16.30-20.30 \nRoom :614 \nFloor: 6  \nPhone: 212 ttt tt tt");
                }
                else if ("Prof. Dr. Çetin Erol".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :20.30-00.30 \nRoom :615 \nFloor: 6  \nPhone: 212 vvv vv vv");
                }
                else if ("Prof. Dr. Derviş Oral".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :00.30-07.30 \nRoom :616 \nFloor: 6  \nPhone: 212 uuu uu uu");
                }

                else
                    MessageBox.Show("Please Select One Of The Doctors");

            }
            //Gastroenterology
            else if ("Gastroenterology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Mehmet Akyüz".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :714 \nFloor: 7  \nPhone: xxxxxxxxx");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Gynecology
            else if ("Gynecology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Ayşe Sönmez".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :814 \nFloor: 8  \nPhone: xxxxxxxxx");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Microbiology
            else if ("Microbiology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Sena Özdemir".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :817 \nFloor: 8  \nPhone: xxxxxxxxx");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Nutrition and Dietetics
            else if ("Nutrition and Dietetics".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Merve Korkmaz".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :812 \nFloor: 8  \nPhone: xxxxxxxxx");
                }
                else if ("Sevda Kara".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :12.30-18.30 \nRoom :620 \nFloor: 6  \nPhone: yyyyyyyyyy");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Oncology
            else if ("Oncology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Volkan Çetin".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :320 \nFloor: 3  \nPhone: xxxxxxxxx");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Physiotherapy
            else if ("Physiotherapy".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Tolga Şimşek".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :520 \nFloor: 5  \nPhone: xxxxxxxxx");
                }
                else if ("Sevda Kara".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :12.30-18.30 \nRoom :522 \nFloor: 5  \nPhone: yyyyyyyyyy");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Radiology
            else if ("Radiology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Ece Özkan".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :822 \nFloor: 8  \nPhone: xxxxxxxxx");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Radiotherapy
            else if ("Radiotherapy".Equals(cmbxdepartment.SelectedItem))
            {
                if ("İbrahim Aydın".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :224 \nFloor: 2  \nPhone: xxxxxxxxx");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }
            //Urology
            else if ("Urology".Equals(cmbxdepartment.SelectedItem))
            {
                if ("Ceren Özdemir".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :08.30-16.30 \nRoom :524 \nFloor: 5  \nPhone: xxxxxxxxx");
                }
                else if ("Hakan Çetin".Equals(lbDoctor.SelectedItem))
                {
                    MessageBox.Show(lbDoctor.SelectedItem + "From Department Of " + cmbxdepartment.SelectedItem +
                        " " +
                        " \nWorking Hours :12.30-18.30 \nRoom :622 \nFloor: 6  \nPhone: yyyyyyyyyy");
                }
                else
                    MessageBox.Show("Please Select One Of The Doctors");
            }

            else
                MessageBox.Show("please choose department !!");
            }

        private void cmbxdepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ArrayList drAnest = new ArrayList();
            ArrayList drCardio = new ArrayList();
            ArrayList drOrthopaedics = new ArrayList();
            ArrayList drOtolaryngology = new ArrayList();

            drAnest.Add("Prof. Dr. Ali Yıldırım");
            drAnest.Add("Prof. Dr. Kemal Söyler");
            drAnest.Add("Prof. Dr. Kadir Yılmaz");
            drCardio.Add("Prof. Dr. Gazi Yaşargil");
            drCardio.Add("Prof. Dr. Hande Özdinler");
            drCardio.Add("Prof.Dr. Ömer Özkan");
            drCardio.Add("Dr. Murat Digiçaylıoğlu");
            drOrthopaedics.Add("Prof. Dr. Tayfun Aybek");
            drOrthopaedics.Add("Prof. Dr. Murat Günel");
            drOrthopaedics.Add("Prof. Dr. Tanju Atamer");
            drOrthopaedics.Add("Prof. Dr. Ali Gürçay");
            drOrthopaedics.Add("Prof. Dr. Faruk Aykan");
            drOtolaryngology.Add("Dr. Aydoğan Öbek");
            drOtolaryngology.Add("Dr. Ayda Uluhan");
            drOtolaryngology.Add("Prof. Dr. Ali Oto");
            drOtolaryngology.Add("Prof. Dr. Çetin Erol");
            drOtolaryngology.Add("Prof. Dr. Derviş Oral");

            if ("Anesthetics".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();

                foreach (object dr in drAnest)
                {
                    lbDoctor.Items.Add(dr);
                }
            }

            else if ("Cardiology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();

                foreach (object dr in drCardio)
                {
                    lbDoctor.Items.Add(dr);
                }
            }

            else if ("Orthopaedics".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                foreach (object dr in drOrthopaedics)
                {
                    lbDoctor.Items.Add(dr);
                }
            }

            else if ("Otolaryngology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                foreach (object dr in drOtolaryngology)
                {
                    lbDoctor.Items.Add(dr);
                }
            }

            else if ("Gastroenterology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Mehmet Akyüz");
            }


            else if ("Gynecology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Ayşe Sönmez");
            }

            else if ("Microbiology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Sena Özdemir");
            }

            else if ("Neurology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Mert Kılıç");
                lbDoctor.Items.Add("Ömer Arslan");
            }

            else if ("Nutrition and Dietetics".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Merve Korkmaz");
                lbDoctor.Items.Add("Sevda Kara");
            }

            else if ("Oncology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Volkan Çetin");
            }

            else if ("Physiotherapy".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Tolga Şimşek");
                lbDoctor.Items.Add("Hande Kurt");
            }

            else if ("Radiology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Ece Özkan");
            }

            else if ("Radiotherapy".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("İbrahim Aydın");
            }

            else if ("Urology".Equals(cmbxdepartment.SelectedItem))
            {
                lbDoctor.Items.Clear();
                lbDoctor.Items.Add("Ceren Özdemir");
                lbDoctor.Items.Add("Hakan Çetin");
            }
        }

    }
}