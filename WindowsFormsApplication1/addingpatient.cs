﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.OleDb;
using System.Text.RegularExpressions;


namespace WindowsFormsApplication1
{
    public partial class addingpatient : Form
    {
        

        OleDbConnection Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
        OleDbCommand Command = new OleDbCommand();
        OleDbDataAdapter adtr = new OleDbDataAdapter();
        DataSet ds = new System.Data.DataSet();

        public addingpatient()
        {
            InitializeComponent();
           
        }

        void list ()
        {
            Connection = new OleDbConnection("Provider=Microsoft.ACE.Oledb.12.0;Data Source=database.accdb");
            adtr = new OleDbDataAdapter("SElect *from patientlist", Connection);
            ds = new DataSet();
            Connection.Open();
            adtr.Fill(ds, "patientlist");
            dataGridView1.DataSource = ds.Tables["patientlist"];
            Connection.Close();
        }

        private void addingpatient_Load(object sender, EventArgs e)
        {
            list();
            
        }
        
        private void btnsave_Click(object sender, EventArgs e)
        {
            string email = txtemail.Text;
            
            if (txtid.Text != "" && txtname.Text != "" && txtsurname.Text != "" && comboGender.SelectedItem.ToString() != "" && txtmother.Text != "" && txtfather.Text != "" &&
               txtplace.Text != "" && txtdate.Text != "" && txtcell.Text != "" && txthome.Text != "" && txtemail.Text != "" && txtaddress.Text != "" && comboDepartment.Text != "" && comboDoctor.Text != "" && dateTimeAppointment.Text != "" && listBoxTime.Text != "")
            {
                Regex rule2 = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
                
                if (!rule2.IsMatch(email))
                    MessageBox.Show("Please Enter Valid E-mail Address");

                else
                {
                    Connection.Open();
                    Command.Connection = Connection;
                    Command.CommandText = "Insert Into patientlist(idnumber,patientname,patientsurname,gender,mothername,fathername,placeofbirth,dateofbirth,cellphone,homephone,email,address,department,doctor,appointmentdate,appointmenttime) Values ('" +
                        txtid.Text + "','" + txtname.Text + "','" + txtsurname.Text + "','" + comboGender.SelectedItem.ToString() + "','" + txtmother.Text + "','" + txtfather.Text + "','" + txtplace.Text + "','" + txtdate.Text + "','" +
                        txtcell.Text + "','" + txthome.Text + "','" + txtemail.Text + "','" + txtaddress.Text + "','" + comboDepartment.SelectedItem.ToString() + "','" +
                        comboDoctor.SelectedItem.ToString() + "','" + dateTimeAppointment.Text + "','" + listBoxTime.SelectedItem.ToString() + "')";
                    Command.ExecuteNonQuery();
                    Command.Dispose();
                    Connection.Close();
                    MessageBox.Show("Registration Successful.", "Registration Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Connection.Close();
                    list();
                }
            }
            else
            {
                MessageBox.Show("Please fill in the blanks!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            
        }
        
        private void addingpatient_FormClosed(object sender, FormClosedEventArgs e)
        {
            hospitalms f1 = new hospitalms();
            f1.Show();
        }

        private void txtname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void txtsurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
               && !char.IsSeparator(e.KeyChar);
        }
        private void txtid_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void txtmother_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
               && !char.IsSeparator(e.KeyChar);
        }
        private void txtfather_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
               && !char.IsSeparator(e.KeyChar);
        }
        private void txtcell_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void txthome_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        private void txtplace_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
               && !char.IsSeparator(e.KeyChar);
        }
        

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtdate_Click(object sender, EventArgs e)
        {
            
            
                txtdate.Clear();
            
        }

       

        


    }
}

